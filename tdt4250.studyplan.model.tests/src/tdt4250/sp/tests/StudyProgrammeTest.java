/**
 */
package tdt4250.sp.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.sp.SpFactory;
import tdt4250.sp.StudyProgramme;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Study Programme</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StudyProgrammeTest extends TestCase {

	/**
	 * The fixture for this Study Programme test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyProgramme fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StudyProgrammeTest.class);
	}

	/**
	 * Constructs a new Study Programme test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgrammeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Study Programme test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StudyProgramme fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Study Programme test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyProgramme getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SpFactory.eINSTANCE.createStudyProgramme());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //StudyProgrammeTest
