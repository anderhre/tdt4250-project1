/**
 */
package tdt4250.sp;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.Semester#getYear <em>Year</em>}</li>
 *   <li>{@link tdt4250.sp.Semester#getTerm <em>Term</em>}</li>
 *   <li>{@link tdt4250.sp.Semester#getSpecialization <em>Specialization</em>}</li>
 *   <li>{@link tdt4250.sp.Semester#getMandatoryCourseGroup <em>Mandatory Course Group</em>}</li>
 *   <li>{@link tdt4250.sp.Semester#getElectiveCourseGroup <em>Elective Course Group</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getSemester()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='needsEnoughAvailableStudyPoints'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 needsEnoughAvailableStudyPoints='Sequence{self.mandatoryCourseGroup.o.studyPoints-&gt;sum(), self.mandatoryCourseGroup.m1a.studyPoints-&gt;sum(), self.mandatoryCourseGroup.m2a.studyPoints-&gt;sum(), self.electiveCourseGroup.va.studyPoints-&gt;sum(), self.electiveCourseGroup.vb.studyPoints-&gt;sum()}-&gt;sum() &gt;= 30.0'"
 * @generated
 */
public interface Semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see tdt4250.sp.SpPackage#getSemester_Year()
	 * @model required="true"
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Semester#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Term</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.sp.Term}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Term</em>' attribute.
	 * @see tdt4250.sp.Term
	 * @see #setTerm(Term)
	 * @see tdt4250.sp.SpPackage#getSemester_Term()
	 * @model required="true"
	 * @generated
	 */
	Term getTerm();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Semester#getTerm <em>Term</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Term</em>' attribute.
	 * @see tdt4250.sp.Term
	 * @see #getTerm()
	 * @generated
	 */
	void setTerm(Term value);

	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.sp.Specialization}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' attribute.
	 * @see tdt4250.sp.Specialization
	 * @see #setSpecialization(Specialization)
	 * @see tdt4250.sp.SpPackage#getSemester_Specialization()
	 * @model
	 * @generated
	 */
	Specialization getSpecialization();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Semester#getSpecialization <em>Specialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specialization</em>' attribute.
	 * @see tdt4250.sp.Specialization
	 * @see #getSpecialization()
	 * @generated
	 */
	void setSpecialization(Specialization value);

	/**
	 * Returns the value of the '<em><b>Mandatory Course Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory Course Group</em>' containment reference.
	 * @see #setMandatoryCourseGroup(MandatoryCourseGroup)
	 * @see tdt4250.sp.SpPackage#getSemester_MandatoryCourseGroup()
	 * @model containment="true"
	 * @generated
	 */
	MandatoryCourseGroup getMandatoryCourseGroup();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Semester#getMandatoryCourseGroup <em>Mandatory Course Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mandatory Course Group</em>' containment reference.
	 * @see #getMandatoryCourseGroup()
	 * @generated
	 */
	void setMandatoryCourseGroup(MandatoryCourseGroup value);

	/**
	 * Returns the value of the '<em><b>Elective Course Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elective Course Group</em>' containment reference.
	 * @see #setElectiveCourseGroup(ElectiveCourseGroup)
	 * @see tdt4250.sp.SpPackage#getSemester_ElectiveCourseGroup()
	 * @model containment="true"
	 * @generated
	 */
	ElectiveCourseGroup getElectiveCourseGroup();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Semester#getElectiveCourseGroup <em>Elective Course Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elective Course Group</em>' containment reference.
	 * @see #getElectiveCourseGroup()
	 * @generated
	 */
	void setElectiveCourseGroup(ElectiveCourseGroup value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model courseRequired="true" statusRequired="true"
	 * @generated
	 */
	void addCourse(Course course, Status status);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model specializationRequired="true"
	 * @generated
	 */
	void chooseSpecialization(Specialization specialization);

	
	EList<Course> getMandatory();
	
	EList<Course> getElectives();
} // Semester
