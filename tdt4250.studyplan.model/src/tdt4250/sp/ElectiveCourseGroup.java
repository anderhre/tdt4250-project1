/**
 */
package tdt4250.sp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Elective Course Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.ElectiveCourseGroup#getVa <em>Va</em>}</li>
 *   <li>{@link tdt4250.sp.ElectiveCourseGroup#getVb <em>Vb</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getElectiveCourseGroup()
 * @model
 * @generated
 */
public interface ElectiveCourseGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Va</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Va</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getElectiveCourseGroup_Va()
	 * @model
	 * @generated
	 */
	EList<Course> getVa();

	/**
	 * Returns the value of the '<em><b>Vb</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vb</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getElectiveCourseGroup_Vb()
	 * @model
	 * @generated
	 */
	EList<Course> getVb();

} // ElectiveCourseGroup
