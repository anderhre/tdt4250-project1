package tdt4250.sp.example;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import tdt4250.sp.Department;
import tdt4250.sp.SpPackage;

public class RunValidation {

	public static void main(String[] args) {
		ResourceSet resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(SpPackage.eNS_URI, SpPackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		
		Resource resource = resSet.getResource(URI.createURI(PrintDynamicInstance.class.getResource("studyPlan.xmi").toString()), true);
	
		Department root = (Department) resource.getContents().get(0);
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(root);
		//System.out.println(diagnostics.getSeverity());
		System.out.println(diagnostics);
	}
}
