/**
 */
package tdt4250.sp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mandatory Course Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.MandatoryCourseGroup#getO <em>O</em>}</li>
 *   <li>{@link tdt4250.sp.MandatoryCourseGroup#getM1a <em>M1a</em>}</li>
 *   <li>{@link tdt4250.sp.MandatoryCourseGroup#getM2a <em>M2a</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getMandatoryCourseGroup()
 * @model
 * @generated
 */
public interface MandatoryCourseGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>O</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>O</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getMandatoryCourseGroup_O()
	 * @model
	 * @generated
	 */
	EList<Course> getO();

	/**
	 * Returns the value of the '<em><b>M1a</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>M1a</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getMandatoryCourseGroup_M1a()
	 * @model
	 * @generated
	 */
	EList<Course> getM1a();

	/**
	 * Returns the value of the '<em><b>M2a</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.sp.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>M2a</em>' reference list.
	 * @see tdt4250.sp.SpPackage#getMandatoryCourseGroup_M2a()
	 * @model
	 * @generated
	 */
	EList<Course> getM2a();

} // MandatoryCourseGroup
