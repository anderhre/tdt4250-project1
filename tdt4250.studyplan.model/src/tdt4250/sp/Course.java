/**
 */
package tdt4250.sp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.Course#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250.sp.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.sp.Course#getStudyPoints <em>Study Points</em>}</li>
 *   <li>{@link tdt4250.sp.Course#getOwnedBy <em>Owned By</em>}</li>
 *   <li>{@link tdt4250.sp.Course#getLevel <em>Level</em>}</li>
 * </ul>
 *
 * @see tdt4250.sp.SpPackage#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='studyPointRange studyPointsNonZero'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 studyPointsNonZero='self.studyPoints-&gt;sum() &gt; 0.0'"
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see tdt4250.sp.SpPackage#getCourse_Code()
	 * @model required="true"
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.sp.SpPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Study Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Study Points</em>' attribute.
	 * @see #setStudyPoints(float)
	 * @see tdt4250.sp.SpPackage#getCourse_StudyPoints()
	 * @model required="true"
	 * @generated
	 */
	float getStudyPoints();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Course#getStudyPoints <em>Study Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Study Points</em>' attribute.
	 * @see #getStudyPoints()
	 * @generated
	 */
	void setStudyPoints(float value);

	/**
	 * Returns the value of the '<em><b>Owned By</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.sp.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned By</em>' container reference.
	 * @see #setOwnedBy(Department)
	 * @see tdt4250.sp.SpPackage#getCourse_OwnedBy()
	 * @see tdt4250.sp.Department#getCourses
	 * @model opposite="courses" required="true" transient="false"
	 * @generated
	 */
	Department getOwnedBy();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Course#getOwnedBy <em>Owned By</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owned By</em>' container reference.
	 * @see #getOwnedBy()
	 * @generated
	 */
	void setOwnedBy(Department value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.sp.CourseLevel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see tdt4250.sp.CourseLevel
	 * @see #setLevel(CourseLevel)
	 * @see tdt4250.sp.SpPackage#getCourse_Level()
	 * @model required="true"
	 * @generated
	 */
	CourseLevel getLevel();

	/**
	 * Sets the value of the '{@link tdt4250.sp.Course#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see tdt4250.sp.CourseLevel
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(CourseLevel value);

} // Course
