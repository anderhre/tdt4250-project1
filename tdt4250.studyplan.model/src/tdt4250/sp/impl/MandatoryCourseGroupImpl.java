/**
 */
package tdt4250.sp.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import tdt4250.sp.Course;
import tdt4250.sp.MandatoryCourseGroup;
import tdt4250.sp.SpPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mandatory Course Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.sp.impl.MandatoryCourseGroupImpl#getO <em>O</em>}</li>
 *   <li>{@link tdt4250.sp.impl.MandatoryCourseGroupImpl#getM1a <em>M1a</em>}</li>
 *   <li>{@link tdt4250.sp.impl.MandatoryCourseGroupImpl#getM2a <em>M2a</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MandatoryCourseGroupImpl extends MinimalEObjectImpl.Container implements MandatoryCourseGroup {
	/**
	 * The cached value of the '{@link #getO() <em>O</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getO()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> o;

	/**
	 * The cached value of the '{@link #getM1a() <em>M1a</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getM1a()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> m1a;

	/**
	 * The cached value of the '{@link #getM2a() <em>M2a</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getM2a()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> m2a;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MandatoryCourseGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpPackage.Literals.MANDATORY_COURSE_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getO() {
		if (o == null) {
			o = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.MANDATORY_COURSE_GROUP__O);
		}
		return o;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getM1a() {
		if (m1a == null) {
			m1a = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.MANDATORY_COURSE_GROUP__M1A);
		}
		return m1a;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getM2a() {
		if (m2a == null) {
			m2a = new EObjectResolvingEList<Course>(Course.class, this, SpPackage.MANDATORY_COURSE_GROUP__M2A);
		}
		return m2a;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SpPackage.MANDATORY_COURSE_GROUP__O:
				return getO();
			case SpPackage.MANDATORY_COURSE_GROUP__M1A:
				return getM1a();
			case SpPackage.MANDATORY_COURSE_GROUP__M2A:
				return getM2a();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SpPackage.MANDATORY_COURSE_GROUP__O:
				getO().clear();
				getO().addAll((Collection<? extends Course>)newValue);
				return;
			case SpPackage.MANDATORY_COURSE_GROUP__M1A:
				getM1a().clear();
				getM1a().addAll((Collection<? extends Course>)newValue);
				return;
			case SpPackage.MANDATORY_COURSE_GROUP__M2A:
				getM2a().clear();
				getM2a().addAll((Collection<? extends Course>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SpPackage.MANDATORY_COURSE_GROUP__O:
				getO().clear();
				return;
			case SpPackage.MANDATORY_COURSE_GROUP__M1A:
				getM1a().clear();
				return;
			case SpPackage.MANDATORY_COURSE_GROUP__M2A:
				getM2a().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SpPackage.MANDATORY_COURSE_GROUP__O:
				return o != null && !o.isEmpty();
			case SpPackage.MANDATORY_COURSE_GROUP__M1A:
				return m1a != null && !m1a.isEmpty();
			case SpPackage.MANDATORY_COURSE_GROUP__M2A:
				return m2a != null && !m2a.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MandatoryCourseGroupImpl
