# TDT4250 Project 1

## Repo structure
The repository is an EMF project consisting of the three folders `model`, `tests` and `edit`.

### Model
Contains an Ecore-model, a dynamic instance of the model (XMI), and a genmodel used to generate model code and test code.

#### Ecore-model
A short description of how some assignment demands for the model is solved, is given below:
* <b>Department</b>-class has a derived feature `noOfCourses`, opposite-references to <b>StudyProgramme</b> and <b>Course</b> and an EOperation `addCourse`.
* <b>Semester</b>-class has an OCL-constraint checking whether 30+ studypoints are offered through courses, and has the EOperations `chooseSpecialization` and `addCourse`.
* <b>Course</b>-class has a manually written constraint checking whether the studypoints of a course is between 0 and 30, and an OCL-constraint checking whether studypoints are more than 0.

#### XMI
A dynamic instance of the Ecore-model, used to validate the model.
It intentionally has one valid semester, one invalid semester and one invalid course to test/show that the constraints work as expected.

#### Genmodel
Used to generate model code and test code.

### Tests
Contains generated test code as well as four manually written tests 
* `testGetNoOfCourses` and `testAddCourse__Course` in `DepartmentTest.java`
* `testAddCourse__Course_Status` and `testChooseSpecialization__Specialization` in `SemesterTest.java` 

